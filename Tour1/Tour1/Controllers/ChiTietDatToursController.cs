﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Tour1.Models;

namespace Tour1.Controllers
{
    public class ChiTietDatToursController : Controller
    {
        private TourContext db = new TourContext();

        // GET: ChiTietDatTours
        public ActionResult Index()
        {
            var chiTietDatTours = db.ChiTietDatTours.Include(c => c.Tour).Include(c => c.DatTour);
            return View(chiTietDatTours.ToList());
        }

        // GET: ChiTietDatTours/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChiTietDatTour chiTietDatTour = db.ChiTietDatTours.Find(id);
            if (chiTietDatTour == null)
            {
                return HttpNotFound();
            }
            return View(chiTietDatTour);
        }

        // GET: ChiTietDatTours/Create
        public ActionResult Create()
        {
            ViewBag.MaTour = new SelectList(db.Tours, "MaTour", "TenTour");
            ViewBag.MaDat = new SelectList(db.DatTours, "MaDat", "MaDat");
            return View();
        }

        // POST: ChiTietDatTours/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MaDat,MaTour,NgayKhoiHanh,SoLuongKhach,LoaiKS")] ChiTietDatTour chiTietDatTour)
        {
            if (ModelState.IsValid)
            {
                db.ChiTietDatTours.Add(chiTietDatTour);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MaTour = new SelectList(db.Tours, "MaTour", "TenTour", chiTietDatTour.MaTour);
            ViewBag.MaDat = new SelectList(db.DatTours, "MaDat", "MaDat", chiTietDatTour.MaDat);
            return View(chiTietDatTour);
        }

        // GET: ChiTietDatTours/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChiTietDatTour chiTietDatTour = db.ChiTietDatTours.Find(id);
            if (chiTietDatTour == null)
            {
                return HttpNotFound();
            }
            ViewBag.MaTour = new SelectList(db.Tours, "MaTour", "TenTour", chiTietDatTour.MaTour);
            ViewBag.MaDat = new SelectList(db.DatTours, "MaDat", "MaDat", chiTietDatTour.MaDat);
            return View(chiTietDatTour);
        }

        // POST: ChiTietDatTours/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MaDat,MaTour,NgayKhoiHanh,SoLuongKhach,LoaiKS")] ChiTietDatTour chiTietDatTour)
        {
            if (ModelState.IsValid)
            {
                db.Entry(chiTietDatTour).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MaTour = new SelectList(db.Tours, "MaTour", "TenTour", chiTietDatTour.MaTour);
            ViewBag.MaDat = new SelectList(db.DatTours, "MaDat", "MaDat", chiTietDatTour.MaDat);
            return View(chiTietDatTour);
        }

        // GET: ChiTietDatTours/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChiTietDatTour chiTietDatTour = db.ChiTietDatTours.Find(id);
            if (chiTietDatTour == null)
            {
                return HttpNotFound();
            }
            return View(chiTietDatTour);
        }

        // POST: ChiTietDatTours/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ChiTietDatTour chiTietDatTour = db.ChiTietDatTours.Find(id);
            db.ChiTietDatTours.Remove(chiTietDatTour);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
