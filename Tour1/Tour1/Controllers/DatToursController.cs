﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Tour1.Models;

namespace Tour1.Controllers
{
    public class DatToursController : Controller
    {
        private TourContext db = new TourContext();

        // GET: DatTours
        public ActionResult Index()
        {
            var datTours = db.DatTours.Include(d => d.ChiTietDatTour).Include(d => d.ThanhVien);
            return View(datTours.ToList());
        }

        // GET: DatTours/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DatTour datTour = db.DatTours.Find(id);
            if (datTour == null)
            {
                return HttpNotFound();
            }
            return View(datTour);
        }

        // GET: DatTours/Create
        public ActionResult Create()
        {
            ViewBag.MaDat = new SelectList(db.ChiTietDatTours, "MaDat", "LoaiKS");
            ViewBag.MaTV = new SelectList(db.ThanhViens, "MaTV", "Username");
            return View();
        }

        // POST: DatTours/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MaDat,MaTV,NgayDat")] DatTour datTour)
        {
            if (ModelState.IsValid)
            {
                db.DatTours.Add(datTour);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MaDat = new SelectList(db.ChiTietDatTours, "MaDat", "LoaiKS", datTour.MaDat);
            ViewBag.MaTV = new SelectList(db.ThanhViens, "MaTV", "Username", datTour.MaTV);
            return View(datTour);
        }

        // GET: DatTours/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DatTour datTour = db.DatTours.Find(id);
            if (datTour == null)
            {
                return HttpNotFound();
            }
            ViewBag.MaDat = new SelectList(db.ChiTietDatTours, "MaDat", "LoaiKS", datTour.MaDat);
            ViewBag.MaTV = new SelectList(db.ThanhViens, "MaTV", "Username", datTour.MaTV);
            return View(datTour);
        }

        // POST: DatTours/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MaDat,MaTV,NgayDat")] DatTour datTour)
        {
            if (ModelState.IsValid)
            {
                db.Entry(datTour).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MaDat = new SelectList(db.ChiTietDatTours, "MaDat", "LoaiKS", datTour.MaDat);
            ViewBag.MaTV = new SelectList(db.ThanhViens, "MaTV", "Username", datTour.MaTV);
            return View(datTour);
        }

        // GET: DatTours/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DatTour datTour = db.DatTours.Find(id);
            if (datTour == null)
            {
                return HttpNotFound();
            }
            return View(datTour);
        }

        // POST: DatTours/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DatTour datTour = db.DatTours.Find(id);
            db.DatTours.Remove(datTour);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
