﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Tour5.Models;

namespace Tour5.Controllers
{
    public class CommentsController : Controller
    {
        private Tour5Context db = new Tour5Context();

        // GET: Comments
        public ActionResult Index()
        {
            var comments = db.Comments.Include(c => c.ThanhVien).Include(c => c.Tour);
            return View(comments.ToList());
        }

        // GET: Comments/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        // GET: Comments/Create
        public ActionResult Create()
        {
            ViewBag.MaTV = new SelectList(db.ThanhViens, "MaTV", "Username");
            ViewBag.MaTour = new SelectList(db.Tours, "MaTour", "TenTour");
            return View();
        }

        // POST: Comments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MaCmt,MaTV,MaTour,NoiDungCmt,Vote")] Comment comment)
        {
            if (ModelState.IsValid)
            {
                db.Comments.Add(comment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MaTV = new SelectList(db.ThanhViens, "MaTV", "Username", comment.MaTV);
            ViewBag.MaTour = new SelectList(db.Tours, "MaTour", "TenTour", comment.MaTour);
            return View(comment);
        }

        // GET: Comments/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            ViewBag.MaTV = new SelectList(db.ThanhViens, "MaTV", "Username", comment.MaTV);
            ViewBag.MaTour = new SelectList(db.Tours, "MaTour", "TenTour", comment.MaTour);
            return View(comment);
        }

        // POST: Comments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MaCmt,MaTV,MaTour,NoiDungCmt,Vote")] Comment comment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(comment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MaTV = new SelectList(db.ThanhViens, "MaTV", "Username", comment.MaTV);
            ViewBag.MaTour = new SelectList(db.Tours, "MaTour", "TenTour", comment.MaTour);
            return View(comment);
        }

        // GET: Comments/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        // POST: Comments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Comment comment = db.Comments.Find(id);
            db.Comments.Remove(comment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
