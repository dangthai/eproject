//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tour5.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tour
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Tour()
        {
            this.Comments = new HashSet<Comment>();
            this.ChiTietDats = new HashSet<ChiTietDat>();
        }
    
        public int MaTour { get; set; }
        public int MaLoai { get; set; }
        public string TenTour { get; set; }
        public string GtTour { get; set; }
        public string NoiDungTour { get; set; }
        public System.DateTime NgayKhoiHanh { get; set; }
        public string DiemKhoiHanh { get; set; }
        public System.DateTime NgayThem { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual LoaiTour LoaiTour { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ChiTietDat> ChiTietDats { get; set; }
    }
}
